
class Tenant(object):
    def __init__(self, id, name, token):
        self.id = id
        self.name = name
        self.token = token

_tenants = {
    'token1': Tenant(1, 'tenant 1', 'token1'),
    'token2': Tenant(2, 'tenant 2', 'token2')
}

def get_tenant(token):
    return _tenants.get(token)

