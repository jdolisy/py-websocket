"""
The flask application package.
"""

from threading import Thread
from flask import Flask
from flask_socketio import SocketIO
from flask_restful import Api

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'secret!'

api = Api(app)

socketio = SocketIO(app)

thread = None

import server.views

##
## Actually setup the Api resource routing here
##
api.add_resource(views.Asset, '/asset')
