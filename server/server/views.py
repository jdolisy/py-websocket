from gevent import monkey
monkey.patch_all()

import time
import random
import json
from threading import Thread
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, disconnect
from flask_restful import Resource

from server import api, app, thread, socketio
from auth import get_tenant

assets = {
    1: [{'app': 'iris-queue-01', 'cpu': 0, 'memory':0, 'tenant': 1},
        {'app': 'iris-poller', 'cpu': 0,'memory': 0, 'tenant': 1},
        {'app': 'iris-redis-01', 'cpu': 0, 'memory': 0, 'tenant': 1},
        {'app': 'PingdomOS', 'cpu': 0, 'memory': 0, 'tenant': 1},
        {'app': 'py1', 'cpu': 0, 'memory': 0, 'tenant': 1}],
    2: [{'app': 'another-app-01', 'cpu': 0, 'memory': 0, 'tenant': 2},
        {'app': 'another-app-02', 'cpu': 0, 'memory': 0, 'tenant': 2}]
}

def background_thread():
    random.seed()

    while True:
        time.sleep(10)

        print 'rooms: ', str(socketio.rooms)

        rooms = socketio.rooms.get('/iris')
        if not rooms:
            continue

        for tenant_id in rooms.keys():
            
            for asset in assets[tenant_id]:
                asset['cpu'] = random.gauss(.5, .2)
                asset['memory'] = random.gauss(1000, 50)

            socketio.emit('app_summary_update',
                          assets[tenant_id],
                          namespace='/iris',
                          room=tenant_id)

class Asset(Resource):
    def get(self):
        return "Default Resource"

@app.route('/')
def index():
    global thread
    if thread is None:
        thread = Thread(target=background_thread)
        thread.start()
    return render_template('index.html')

@socketio.on('connect', namespace='/iris')
def connect():
    print('Client connected: ' + request.namespace.socket.sessid)
    request.namespace.socket.tenant_id = None

@socketio.on('disconnect', namespace='/iris')
def disconnect():
    print('Client disconnected: ' + request.namespace.socket.sessid)

    if request.namespace.socket.tenant_id:
        # copy currently authenticated tenant
        tenant_id = request.namespace.socket.tenant_id
    
        request.namespace.socket.tenant_id = None
        leave_room(tenant_id)   
        
@socketio.on('authenticate', namespace='/iris')
def authenticate(message):
    print('Client authenticating: ' + request.namespace.socket.sessid)

    if not request.namespace.socket.tenant_id:
        tenant = get_tenant(message['token'])
        if tenant:
            request.namespace.socket.tenant_id = tenant.id

            join_room(tenant.id)

